# frozen_string_literal: true

ENV['DATABASE_URL'] = 'sqlite3:db/example.db' # 'sqlite3::memory:'

require 'bundler/inline'

gemfile true, quiet: true do
  source 'https://rubygems.org'
  gem 'rails', '~> 7.1.0'

  gem 'hirb'
  gem 'pry'
  gem 'sqlite3'
end

require 'rails/all'

class Demo < Rails::Application
  config.load_defaults 7.0

  config.active_storage.service                = :local
  config.active_storage.service_configurations = { local: { root: './storage', service: 'Sqlar' } }
  config.active_support.report_deprecations    = false
  config.eager_load                            = true
  config.time_zone                             = 'Central Time (US & Canada)'

  credentials.secret_key_base = 'It is a secret to everybody!!!'

  # log to stdout
  logger           = ActiveSupport::Logger.new $stdout
  logger.formatter = config.log_formatter
  config.logger    = ActiveSupport::TaggedLogging.new logger
end

Rails.application.initialize!

# silence migrations
ActiveRecord::Migration.verbose = false
ar_logger = ActiveRecord::Base.logger
ActiveRecord::Base.logger = nil

ActiveRecord::Schema.define do
  raw = connection.instance_variable_get '@raw_connection'
  raw.enable_load_extension true
  raw.load_extension 'db/sqlar.dylib'

  create_table :active_storage_blobs, if_not_exists: true do |t|
    t.string   :key,          null: false
    t.string   :filename,     null: false
    t.string   :content_type
    t.text     :metadata
    t.string   :service_name, null: false
    t.bigint   :byte_size,    null: false
    t.string   :checksum
    t.datetime :created_at,   null: false, precision: 6

    t.index [ :key ], unique: true
  end

  create_table :active_storage_attachments, if_not_exists: true do |t|
    t.string     :name,       null: false
    t.references :record,     null: false, polymorphic: true, index: false
    t.references :blob,       null: false
    t.datetime   :created_at, null: false, precision: 6

    t.index [ :record_type, :record_id, :name, :blob_id ],
            name: :index_active_storage_attachments_uniqueness,
            unique: true

    t.foreign_key :active_storage_blobs, column: :blob_id
  end

  create_table :active_storage_variant_records, if_not_exists: true do |t|
    t.belongs_to :blob,             null: false, index: false
    t.string     :variation_digest, null: false

    t.index [ :blob_id, :variation_digest ],
            name: :index_active_storage_variant_records_uniqueness,
            unique: true

    t.foreign_key :active_storage_blobs, column: :blob_id
  end

  create_table :uploads, if_not_exists: true do |t|
    t.timestamps null: false, precision: 6
  end

  create_table :sqlar, if_not_exists: true do |t|
    t.text    :name
    t.integer :mode, default: 0
    t.integer :mtime
    t.integer :sz
    t.blob    :data
  end
end
ActiveRecord::Base.logger = ar_logger # re-enable query logging

class DemoRecord < ActiveRecord::Base
  primary_abstract_class
end

class Sqlar < DemoRecord
  self.table_name = 'sqlar'
  PERMISSIONS = %w[ --- --x -w- -wx r-- r-x rw- rwx ].freeze

  alias_attribute :size, :sz

  def blob
    @blob ||= compressed? ? decompress : data
  end

  def compressed?
    data.size != sz
  rescue ActiveModel::MissingAttributeError # don't error if data wasn't fetched
    true
  end

  def modified_at
    return nil unless mtime?
    @modified_at ||= Time.at mtime
  end
  # Use bitwise operations to extract permissions
  def permissions
    @permissions ||= <<-STR.gsub(/\s/, '')
      #{ PERMISSIONS[(mode >> 6) & 7] }
      #{ PERMISSIONS[(mode >> 3) & 7] }
      #{ PERMISSIONS[mode & 7] }
    STR
  end

private

  def decompress
    self.class.select('sqlar_uncompress(data,sz) as raw')
              .find_by(name: name)
              .raw
  end

end

class Upload < DemoRecord
  has_one_attached :upload
end

# unless Upload.any?
#   path = Pathname.new File.expand_path __FILE__
#   u = Upload.new
#   u.upload.attach content_type: 'text/x-ruby',
#                   filename: path.basename,
#                   io: File.open(path)
#   u.save!
# end
#
# require 'pry'; binding.pry;
#
# Upload.all
# ActiveStorage::Attachment.all
# ActiveStorage::Blob.all

require 'minitest/autorun'
require 'test/service/shared_service_tests'

# https://github.com/rails/rails/blob/a4d5f1d7399a1eb4823a91a6493f0f02f60994ae/activestorage/test/service/disk_service_test.rb
class ActiveStorage::Service::SqlarServiceTest < ActiveSupport::TestCase

  tmp_config = {
    tmp: { service: 'Sqlar', root: File.join(Dir.tmpdir, 'active_storage') }
  }

  SERVICE = ActiveStorage::Service.configure :tmp, tmp_config

  include ActiveStorage::Service::SharedServiceTests

  test "name" do
    assert_equal :tmp, @service.name
  end

  test "url_for_direct_upload" do
    original_url_options = Rails.application.routes.default_url_options.dup
    Rails.application.routes.default_url_options.merge!(protocol: "http", host: "test.example.com", port: 3001)

    key      = SecureRandom.base58(24)
    data     = "Something else entirely!"
    checksum = Digest::MD5.base64digest(data)
    begin
      assert_match(/^https:\/\/example.com\/rails\/active_storage\/disk\/.*$/,
        @service.url_for_direct_upload(key, expires_in: 5.minutes, content_type: "text/plain", content_length: data.size, checksum: checksum))
    ensure
      Rails.application.routes.default_url_options = original_url_options
    end
  end

  test "URL generation" do
    original_url_options = Rails.application.routes.default_url_options.dup
    Rails.application.routes.default_url_options.merge!(protocol: "http", host: "test.example.com", port: 3001)
    begin
      assert_match(/^https:\/\/example.com\/rails\/active_storage\/disk\/.*\/avatar\.png$/,
        @service.url(@key, expires_in: 5.minutes, disposition: :inline, filename: ActiveStorage::Filename.new("avatar.png"), content_type: "image/png"))
    ensure
      Rails.application.routes.default_url_options = original_url_options
    end
  end

  test "URL generation without ActiveStorage::Current.url_options set" do
    ActiveStorage::Current.url_options = nil

    error = assert_raises ArgumentError do
      @service.url(@key, expires_in: 5.minutes, disposition: :inline, filename: ActiveStorage::Filename.new("avatar.png"), content_type: "image/png")
    end

    assert_equal("Cannot generate URL for avatar.png using Disk service, please set ActiveStorage::Current.url_options.", error.message)
  end

  test "URL generation keeps working with ActiveStorage::Current.host set" do
    ActiveStorage::Current.url_options = { host: "https://example.com" }

    original_url_options = Rails.application.routes.default_url_options.dup
    Rails.application.routes.default_url_options.merge!(protocol: "http", host: "test.example.com", port: 3001)
    begin
      assert_match(/^http:\/\/example.com:3001\/rails\/active_storage\/disk\/.*\/avatar\.png$/,
        @service.url(@key, expires_in: 5.minutes, disposition: :inline, filename: ActiveStorage::Filename.new("avatar.png"), content_type: "image/png"))
    ensure
      Rails.application.routes.default_url_options = original_url_options
    end
  end

  test "headers_for_direct_upload generation" do
    assert_equal({ "Content-Type" => "application/json" }, @service.headers_for_direct_upload(@key, content_type: "application/json"))
  end

  test "root" do
    assert_equal tmp_config.dig(:tmp, :root), @service.root
  end

  test "can change root" do
    tmp_path_2 = File.join(Dir.tmpdir, "active_storage_2")
    @service.root = tmp_path_2

    assert_equal tmp_path_2, @service.root
  ensure
    @service.root = tmp_config.dig(:tmp, :root)
  end

end


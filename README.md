# active_storage-Sqlar

Brainstorming about an sqlar service for active storage.

Mostly as a sandbox for learning about the active storage internals

- [overview](https://guides.rubyonrails.org/v7.1.0/active_storage_overview.html)
- [service](https://github.com/rails/rails/tree/main/activestorage/lib/active_storage/service)
- [blob](https://github.com/rails/rails/tree/main/activestorage/app/models/active_storage/blob)

and sqlar

- [docs](https://sqlite.org/sqlar.html)
- [cli](https://sqlite.org/cli.html#sqlar)
- [precompiled sqlar extension](https://github.com/nalgeon/sqlean/releases/incubator)

using a "[single file](https://guides.rubyonrails.org/v7.1.0/contributing_to_ruby_on_rails.html#create-an-executable-test-case)" rails app

```$ ruby -I . sqlar.rb```

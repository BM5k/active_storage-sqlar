require "active_storage/service/disk_service"

module ActiveStorage
  class Service::SqlarService < Service::DiskService

    # def compose(source_keys, destination_key, **)
    #   # File.open(make_path_for(destination_key), "w") do |destination_file|
    #   #   source_keys.each do |source_key|
    #   #     File.open(path_for(source_key), "rb") do |source_file|
    #   #       IO.copy_stream(source_file, destination_file)
    #   #     end
    #   #   end
    #   # end
    # end

    # def delete(key)
    #   # instrument :delete, key: key do
    #   #   File.delete path_for(key)
    #   # rescue Errno::ENOENT
    #   #   # Ignore files already deleted
    #   # end
    # end

    # def delete_prefixed(prefix)
    #   # instrument :delete_prefixed, prefix: prefix do
    #   #   Dir.glob(path_for("#{prefix}*")).each do |path|
    #   #     FileUtils.rm_rf(path)
    #   #   end
    #   # end
    # end

    # def download(key, &block)
    #   # if block_given?
    #   #   instrument :streaming_download, key: key do
    #   #     stream key, &block
    #   #   end
    #   # else
    #   #   instrument :download, key: key do
    #   #     File.binread path_for(key)
    #   #   rescue Errno::ENOENT
    #   #     raise ActiveStorage::FileNotFoundError
    #   #   end
    #   # end
    # end

    # def download_chunk(key, range)
    #   # instrument :download_chunk, key: key, range: range do
    #   #   File.open(path_for(key), "rb") do |file|
    #   #     file.seek range.begin
    #   #     file.read range.size
    #   #   end
    #   # rescue Errno::ENOENT
    #   #   raise ActiveStorage::FileNotFoundError
    #   # end
    # end

    # def exist?(key)
    #   # instrument :exist, key: key do |payload|
    #   #   answer = File.exist? path_for(key)
    #   #   payload[:exist] = answer
    #   #   answer
    #   # end
    # end

    # def upload(key, io, checksum: nil, **)
    #   # instrument :upload, key: key, checksum: checksum do
    #   #   IO.copy_stream(io, make_path_for(key))
    #   #   ensure_integrity_of(key, checksum) if checksum
    #   # end
    # end

  private

    # def stream(key)
    # #   File.open(path_for(key), "rb") do |file|
    # #     while data = file.read(5.megabytes)
    # #       yield data
    # #     end
    # #   end
    # # rescue Errno::ENOENT
    # #   raise ActiveStorage::FileNotFoundError
    # end

    # def ensure_integrity_of(key, checksum)
    #   # unless OpenSSL::Digest::MD5.file(path_for(key)).base64digest == checksum
    #   #   delete key
    #   #   raise ActiveStorage::IntegrityError
    #   # end
    # end

  end
end
